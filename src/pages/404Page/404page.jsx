import React from 'react'

const Pagenotfound = () => {
    return (
        <div className="flex items-center justify-center min-h-screen bg-zinc-100 dark:bg-zinc-900 mt-[-50px]">
            <div className="text-center">
                <h1 className="text-6xl font-bold text-zinc-800 dark:text-white animate-bounce ">404!</h1>
                <p className="text-xl mt-4 text-zinc-600 dark:text-zinc-300">Oops! You're lost.</p>
                <p className="mt-2 text-zinc-500 dark:text-zinc-400  " >The page you are looking for was not found.</p>
                <button
                    className=" mt-6 bg-zinc-800 text-white px-4 py-2 rounded hover:bg-zinc-700 focus:outline-none focus:ring-2 focus:ring-zinc-600 focus:ring-opacity-50"
                >
                    Back to Home
                </button>
            </div>
        </div>

    )
}

export default Pagenotfound