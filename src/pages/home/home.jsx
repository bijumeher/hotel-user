import { React, useState } from 'react'
import Carousel from 'react-bootstrap/Carousel';
import img1 from '../../assets/images/food2.png'
import img2 from '../../assets/images/food4.png'
import img3 from '../../assets/images/food6.png'
import { data } from '../../data/data';

const Home = () => {

    const [foods, setFoods] = useState(data);

    //   Filter Type burgers/pizza/etc
    const filterType = (category) => {
        setFoods(
            data.filter((item) => {
                return item.category === category;
            })
        );
    };

    //   Filter by price
    const filterPrice = (price) => {
        setFoods(
            data.filter((item) => {
                return item.price === price;
            })
        );
    };
    return (
        <div>
            {/* carousel section start */}
            <Carousel data-bs-theme="dark" className='mt-[-50px]'>
                <Carousel.Item interval={1500} >
                    <img
                        className="d-block w-100"
                        src={img1}
                        alt="First slide"
                    />

                </Carousel.Item>
                <Carousel.Item interval={1500} >
                    <img
                        className="d-block w-100"
                        src={img2}
                        alt="Second slide"
                    />

                </Carousel.Item>
                <Carousel.Item interval={1500} >
                    <img
                        className="d-block w-100"
                        src={img3}
                        alt="Third slide"
                    />

                </Carousel.Item>
            </Carousel>
            {/* carousel section end */}

            {/* headline card section */}
            <div className='max-w-[1640px] mx-auto p-4 py-12 grid md:grid-cols-3 gap-6 '>
                {/* Card */}
                <div className='relative rounded-xl'>
                    {/* Overlay */}
                    <div className='absolute w-full h-full text-white bg-black/50 rounded-xl'>
                        <p className='px-2 pt-4 text-2xl font-bold'>Sun's Out, BOGO's Out</p>
                        <p className='px-2'>Through 8/26</p>
                        <button className='absolute mx-2 text-black bg-white border-white bottom-4'>Order Now</button>
                    </div>
                    <img
                        className='max-h-[160px] md:max-h-[200px] w-full object-cover rounded-xl'
                        src='https://images.unsplash.com/photo-1613769049987-b31b641f25b1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjF8fGJyZWFrZmFzdHxlbnwwfDB8MHx8&auto=format&fit=crop&w=800&q=60'
                        alt='/'
                    />
                </div>
                {/* Card */}
                <div className='relative rounded-xl'>
                    {/* Overlay */}
                    <div className='absolute w-full h-full text-white bg-black/50 rounded-xl'>
                        <p className='px-2 pt-4 text-2xl font-bold'>New Restaurants</p>
                        <p className='px-2'>Added Daily</p>
                        <button className='absolute mx-2 text-black bg-white border-white bottom-4'>Order Now</button>
                    </div>
                    <img
                        className='max-h-[160px] md:max-h-[200px] w-full object-cover rounded-xl'
                        src='https://images.unsplash.com/photo-1544025162-d76694265947?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTV8fGJicXxlbnwwfDB8MHx8&auto=format&fit=crop&w=800&q=60'
                        alt='/'
                    />
                </div>
                {/* Card */}
                <div className='relative rounded-xl'>
                    {/* Overlay */}
                    <div className='absolute w-full h-full text-white bg-black/50 rounded-xl'>
                        <p className='px-2 pt-4 text-2xl font-bold'>We Deliver Desserts Too</p>
                        <p className='px-2'>Tasty Treats</p>
                        <button className='absolute mx-2 text-black bg-white border-white bottom-4'>Order Now</button>
                    </div>
                    <img
                        className='max-h-[160px] md:max-h-[200px] w-full object-cover rounded-xl'
                        src='https://images.unsplash.com/photo-1559715745-e1b33a271c8f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTd8fGRlc3NlcnR8ZW58MHwwfDB8fA%3D%3D&auto=format&fit=crop&w=800&q=60'
                        alt='/'
                    />
                </div>
            </div>
            {/* headline card section end */}
            {/* catagory card section start */}
            <div className='max-w-[1640px] m-auto py-12 bg-gray-200'>
                <h1 className='text-4xl font-bold text-center text-orange-600'>
                    Top Rated Menu Items
                </h1>

                {/* Filter Row */}
                <div className='flex flex-col justify-between lg:flex-row'>
                    {/* Fliter Type */}
                    <div>
                        <p className='font-bold text-gray-700'>Filter Type</p>
                        <div className='flex flex-wrap justfiy-between'>
                            <button
                                onClick={() => setFoods(data)}
                                className='m-1 text-orange-600 bg-black border-orange-600 hover:bg-orange-600 hover:text-white'
                            >
                                All
                            </button>
                            <button
                                onClick={() => filterType('burger')}
                                className='m-1 text-orange-600 border-orange-600 hover:bg-orange-600 hover:text-white'
                            >
                                Burgers
                            </button>
                            <button
                                onClick={() => filterType('pizza')}
                                className='m-1 text-orange-600 border-orange-600 hover:bg-orange-600 hover:text-white'
                            >
                                Pizza
                            </button>
                            <button
                                onClick={() => filterType('salad')}
                                className='m-1 text-orange-600 border-orange-600 hover:bg-orange-600 hover:text-white'
                            >
                                Salads
                            </button>
                            <button
                                onClick={() => filterType('chicken')}
                                className='m-1 text-orange-600 border-orange-600 hover:bg-orange-600 hover:text-white'
                            >
                                Chicken
                            </button>
                        </div>
                    </div>

                    {/* Filter Price */}
                    <div>
                        <p className='font-bold text-gray-700'>Filter Price</p>
                        <div className='flex justify-between max-w-[390px] w-full'>
                            <button
                                onClick={() => filterPrice('$5')}
                                className='m-1 text-orange-600 border-orange-600 hover:bg-orange-600 hover:text-white'
                            >
                                $5
                            </button>
                            <button
                                onClick={() => filterPrice('$15')}
                                className='m-1 text-orange-600 border-orange-600 hover:bg-orange-600 hover:text-white'
                            >
                                $15
                            </button>
                            <button
                                onClick={() => filterPrice('$25')}
                                className='m-1 text-orange-600 border-orange-600 hover:bg-orange-600 hover:text-white'
                            >
                                $25
                            </button>
                            <button
                                onClick={() => filterPrice('$20')}
                                className='m-1 text-orange-600 border-orange-600 hover:bg-orange-600 hover:text-white'
                            >
                                $20
                            </button>
                        </div>
                    </div>
                </div>

                {/* Display foods */}
                <div className='grid grid-cols-2 gap-6 pt-4 lg:grid-cols-4'>
                    {foods.map((item, index) => (
                        <div
                            key={index}
                            className='duration-300 border rounded-lg shadow-lg hover:scale-105'
                        >
                            <img
                                src={item.image}
                                alt={item.name}
                                className='w-full h-[200px] object-cover rounded-t-lg'
                            />
                            <div className='flex justify-between px-2 py-4'>
                                <p className='font-bold'>{item.name}</p>
                                <p>
                                    <span className='p-1 text-white bg-orange-500 rounded-full'>
                                        {item.price}
                                    </span>
                                </p>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            {/* catagory card section end*/}
           
        </div>
    )
}

export default Home
