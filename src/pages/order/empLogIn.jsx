import React from 'react'
import img from '../../assets/images/login.jpg'

const EmpLogIn = ({ setAuthEnticate }) => {
    return (
        <div>
            <div className='flex items-center justify-center min-h-screen bg-gray-50'>
                <div className='flex max-w-3xl p-2 bg-gray-100 shadow-lg rounded-2xl'>
                    <div className='px-16 sm:w-1/2'>
                        <h2 className='text-2xl font-bold text-[#002D74]'>LogIn</h2>
                        <p className='mt-4 text-sm text-[#002D74]'>If you already a member easily login</p>
                        <form action=" " className='flex flex-col gap-4'>
                            <input className='p-2 border rounded-xl' type="text" name='email' placeholder='Email' />
                            <div>
                                <input className='w-full p-2 border rounded-xl' type="text" name='password' placeholder='Password ' />

                            </div>
                            <button onClick={() => setAuthEnticate(false)} className='bg-[#002D74] text-white rounded-xl py-2'>Login</button>
                        </form>
                        <div className='grid items-center grid-cols-3 mt-10 text-gray-400'>
                            <hr className='border-gray-600' />
                            <p className='text-center'>OR</p>
                            <hr className='border-gray-600' />

                        </div>
                    </div>
                    <div className='w-1/2 p-2 '>
                        <img className='hidden bg-cyan-100 rounded-2xl sm:block' src={img} alt="/" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default EmpLogIn