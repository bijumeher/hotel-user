import React from 'react'
import { FaPlus } from "react-icons/fa6";

const OrderBill = () => {
    return (
        <div class="bg-zinc-800 text-white p-6 rounded-lg max-w-4xl mx-auto mb-10">
            <div class="flex justify-between mb-4">
                <div>
                    <p>Table No.</p>
                    <p>Customer Mobile No.</p>
                </div>
                <div>
                    <p>Order No.</p>
                </div>
            </div>
            <div class="overflow-x-auto">
                <table class="min-w-full bg-zinc-700 rounded-lg">
                    <thead>
                        <tr>
                            <th class="py-2 px-4 text-left">Serial no.</th>
                            <th class="py-2 px-4 text-left">Item</th>
                            <th class="py-2 px-4 text-left">No. of Plates</th>
                            <th class="py-2 px-4 text-left">Amount</th>
                            <th class="py-2 px-4 text-left">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="py-2 px-4">1</td>
                            <td class="py-2 px-4">Pizza</td>
                            <td class="py-2 px-4">2</td>
                            <td class="py-2 px-4">6 pieces</td>
                            <td class="py-2 px-4">500</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className='flex justify-between'>
                <div>
                    <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full flex items-center gap-2 mt-2">
                        <FaPlus />Add
                    </button>
                </div>
                <div>
                    <p>SubTotal : $1200</p>
                    <p>Tax-Rate : 18%</p>
                    <p>TotalTax : 216</p>
                </div>
            </div>
        </div>

    )
}

export default OrderBill