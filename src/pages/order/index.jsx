import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useState } from 'react';
import EmpLogIn from './empLogIn';
import style from './style.module.css'
import Orderdialog from './orderdialog';

function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
}


const rows = [
    createData('01', 7609090905, 159," RD0456", 500, "paid"),
    createData('02', 7205256479, 160," RD0786", 400, "unpaid"),
    createData('03', 9348380671, 252," RD0980", 250, "paid"),
    createData('04', 6370654719, 143," RD0235", 300, "unpaid"),
    createData('05', 7609090905, 345," RD0467", 560, "paid"),
];

const OrderNow = () => {
    const [orderDialog, setOrderDialog] = useState(false);
    const handleOrderDialog = () => {
        setOrderDialog(!orderDialog);
    };
    const [authEnticate, setAuthEnticate] = useState(true);
    return (
        <div>
            {authEnticate ? <EmpLogIn setAuthEnticate={setAuthEnticate} /> : <>
                <div className={style.ordrbtn}>
                    <button onClick={handleOrderDialog}>Order Now</button>
                </div>
                <div>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Seriel No</TableCell>
                                    <TableCell align="right">Customer Mob. No.</TableCell>
                                    <TableCell align="right">Table No.</TableCell>
                                    <TableCell align="right">Order No.</TableCell>
                                    <TableCell align="right">Total Amount</TableCell>
                                    <TableCell align="right">Status</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows.map((row) => (
                                    <TableRow
                                        key={row.name}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row">
                                            {row.name}
                                        </TableCell>
                                        <TableCell align="right">{row.calories}</TableCell>
                                        <TableCell align="right">{row.fat}</TableCell>
                                        <TableCell align="right">{row.carbs}</TableCell>
                                        <TableCell align="right">{row.protein}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            </>}



            <div className={style.space}>

            </div>
            <Orderdialog open={orderDialog} handleClose={handleOrderDialog} />
        </div>
    )
}

export default OrderNow