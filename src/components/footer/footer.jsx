
import React from 'react';

const sharedClasses = {
  hoverTextZinc300: 'hover:text-zinc-300',
  bgZinc800: 'bg-zinc-800',
  textWhite: 'text-white',
  p8: 'p-8',
  maxW6xl: 'max-w-6xl',
  mxAuto: 'mx-auto',
  gridCols1: 'grid grid-cols-1',
  gridCols3: 'md:grid-cols-3',
  gap8: 'gap-8',
  textXl: 'text-xl',
  fontBold: 'font-bold',
  mb4: 'mb-4',
  flex: 'flex',
  spaceX4: 'space-x-4',
  fontSemiBold: 'font-semibold',
  spaceY2: 'space-y-2',
  wFull: 'w-full',
  p2: 'p-2',
  bgZinc700: 'bg-zinc-700',
  borderNone: 'border-none',
  rounded: 'rounded',
  bgRed600: 'bg-red-600',
  hoverBgRed700: 'hover:bg-red-700',
  textRed600: 'text-red-600',
};

const Footer = () => {
  return (
    <footer className={`${sharedClasses.bgZinc800} ${sharedClasses.textWhite} ${sharedClasses.p8}`}>
      <div className={`${sharedClasses.maxW6xl} ${sharedClasses.mxAuto} ${sharedClasses.gridCols1} ${sharedClasses.gridCols3} ${sharedClasses.gap8}`}>
        <FooterSection1 />
        <FooterSection2 />
        <FooterSection3 />
      </div>
    </footer>
  );
};

const FooterSection1 = () => {
  return (
    <div>
      <h2 className={`${sharedClasses.textXl} ${sharedClasses.fontBold} ${sharedClasses.mb4}`}>Rahul Dhaba</h2>
      <div className={`${sharedClasses.flex} ${sharedClasses.spaceX4} ${sharedClasses.mb4}`}>
        <SocialLink icon="fab fa-facebook-f" />
        <SocialLink icon="fab fa-twitter" />
        <SocialLink icon="fab fa-youtube" />
      </div>
      <div>
        <h3 className={sharedClasses.fontSemiBold}>Support</h3>
        <ul className={sharedClasses.spaceY2}>
          <li><a href="#" className={sharedClasses.hoverTextZinc300}>Contact Us</a></li>
          <li><a href="#" className={sharedClasses.hoverTextZinc300}>FAQ</a></li>
          <li><a href="#" className={sharedClasses.hoverTextZinc300}>Downloads</a></li>
          <li><a href="#" className={sharedClasses.hoverTextZinc300}>Locate A Dealer</a></li>
          <li><a href="#" className={sharedClasses.hoverTextZinc300}>Product Registration</a></li>
          <li><a href="#" className={sharedClasses.hoverTextZinc300}>Spare Parts</a></li>
        </ul>
      </div>
    </div>
  );
};

const FooterSection2 = () => {
  return (
    <div>
      <h3 className={`${sharedClasses.fontSemiBold} ${sharedClasses.mb4}`}>Rahul Dhaba</h3>
      <ul className={sharedClasses.spaceY2}>
        <li><a href="#" className={sharedClasses.hoverTextZinc300}>About Rahul Dhaba</a></li>
        <li><a href="#" className={sharedClasses.hoverTextZinc300}>Rahul Dhaba Design</a></li>
        <li><a href="#" className={sharedClasses.hoverTextZinc300}>Careers</a></li>
        <li><a href="#" className={sharedClasses.hoverTextZinc300}>Newsroom</a></li>
        <li><a href="#" className={sharedClasses.hoverTextZinc300}>Furrion Access</a></li>
      </ul>
    </div>
  );
};

const FooterSection3 = () => {
  return (
    <div>
      <h3 className={`${sharedClasses.fontSemiBold} ${sharedClasses.mb4}`}>Stay up to date on the latest from Rahul Dhaba</h3>
      <form action="#" method="POST">
        <input type="email" placeholder="Enter your email address" className={`${sharedClasses.wFull} ${sharedClasses.p2} ${sharedClasses.mb4} ${sharedClasses.bgZinc700} ${sharedClasses.borderNone} ${sharedClasses.rounded}`} />
        <button type="submit" className={`${sharedClasses.wFull} ${sharedClasses.bgRed600} ${sharedClasses.hoverBgRed700} ${sharedClasses.textWhite} ${sharedClasses.p2} ${sharedClasses.rounded}`}>Sign Up</button>
      </form>
    </div>
  );
};

const SocialLink = ({ icon }) => {
  return (
    <a href="#" className={sharedClasses.hoverTextZinc300}>
      <i className={icon}></i>
    </a>
  );
};

export default Footer;
