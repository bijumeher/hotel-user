import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

import Home from "./pages/home/home";
import About from "./pages/about/about";
import Menu from "./pages/menu/menu";
import Table from "./pages/table/table";
import OrderNow from "./pages/order";
import Pagenotfound from "./pages/404Page/404page";
import Orderbill from "./pages/order/orderBill";


import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";


const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<App />}>
      <Route path="/*" element={<Pagenotfound />} />
      <Route index element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/menu" element={<Menu />} />
      <Route path="/table" element={<Table />} />
      <Route path="/order" element={<OrderNow />} />
      <Route path="/orderbill" element={<Orderbill/>} />
   

    </Route>
  )
);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

reportWebVitals();
